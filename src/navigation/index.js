import React from 'react';
import { createStackNavigator, createAppContainer, createDrawerNavigator } from "react-navigation";
import { View } from 'react-native';
import NewsListScreen from '../features/news/screens/NewsListScreen';
import NewsDetailsScreen from '../features/news/screens/NewsDetailsScreen';
import BookmarksListScreen from '../features/bookmarks/screens/BookmarksListScreen';
import SetupSourceScreen from '../features/news/screens/SetupSourceScreen';
import Icon from 'react-native-vector-icons/FontAwesome';
import MyText from '../components/MyText';
import BurgerMenuButton from '../components/BurgerMenuButton';

const NewsStack = createStackNavigator({
    NewsList: {
        screen: NewsListScreen,
        navigationOptions: ({navigation}) => ({
            title: 'News',
            headerLeft: (
                <Icon name={'bars'}
                      style={{marginLeft: 10}}
                      size={25}
                      onPress={() => navigation.openDrawer()} />
            )
        })
    },
    NewsDetails: {
        screen: NewsDetailsScreen,
        navigationOptions: () => ({
            title: 'News Details',
        })
    },
    SetupSource: {
        screen: SetupSourceScreen,
        navigationOptions: () => ({
            title: 'SetupSource'
        })
    },
}, {
    // initialRouteName: 'SetupSource',
    initialRouteName: 'NewsList',
    navigationOptions: {
        mode: 'modal'
    }
});

const BookmarksStack = createStackNavigator({
    BookmarksList: {
        screen: BookmarksListScreen,
        navigationOptions: ({navigation}) => ({
            title: 'Bookmarks',
            headerLeft: (
                <Icon name={'bars'}
                      style={{marginLeft: 10}}
                      size={25}
                      onPress={() => navigation.openDrawer()} />
            )
        })
    },
    BookmarksDetails: {
        screen: NewsDetailsScreen,
        navigationOptions: () => ({
            title: 'News Details',
        })
    }
}, {
    initialRouteName: 'BookmarksList',
    mode: 'modal'
});
const DrawerNavigator = createDrawerNavigator({
    NewsStack: {
        screen: NewsStack,
        navigationOptions: () => ({
            title: 'News Feed'
        })
    },
    Bookmarks: {
        screen: BookmarksStack,
        navigationOptions: () => ({
            title: 'Bookmarks'
        })
    }
}, {
    initialRouteName: 'NewsStack'
});

export default createAppContainer(DrawerNavigator);
