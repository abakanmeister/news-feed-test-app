import React, { PureComponent } from 'react';
import { View, Text, Dimensions, StyleSheet } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import colors from '../styles/colors';
import {SSP_SemiBold} from '../styles/fonts';

const { width } = Dimensions.get('window');
export default class OfflineNotice extends React.Component {
    state = {
        isConnected: true
    };
    componentDidMount() {
        NetInfo.addEventListener(state => {
            this.setState({ isConnected: state.isConnected })
        });
    }
    componentWillUnmount() {
        NetInfo.removeEventListener(state => this.handleConnectivityChange);
    }
    handleConnectivityChange = isConnected => this.setState({ isConnected });

    render() {
        const { isConnected } = this.state;
        if (!isConnected)
            return (
                <View style={styles.offlineContainer}>
                    <Text style={styles.offlineText}>! No Internet Connection !</Text>
                </View>);
            return null;
    }
}

const styles = StyleSheet.create({
    offlineContainer: {
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width,
        position: 'absolute',
        top: 20
    },
    offlineText: {
        color: colors.red,
        fontFamily: SSP_SemiBold,
        fontSize: 16
    }
});
