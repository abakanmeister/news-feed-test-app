import React from 'react';
import {Text} from 'react-native';
import {SSP_Regular} from '../styles/fonts';
export default props => <Text {...props}
                              style={[{fontFamily: SSP_Regular, fontSize: 16, color: '#232323'}, props.style]}>{props.children}</Text>
