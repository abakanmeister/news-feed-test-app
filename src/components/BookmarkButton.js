import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import colors from '../styles/colors';

export default class BookmarkButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            iconIsActive: props.iconIsActive
        }
    }
    toggleIcon = () => {
        const { target, removeBookmark, addBookmark } = this.props,
            { iconIsActive } = this.state;
        iconIsActive
            ? removeBookmark(target.id)
            : addBookmark(target);
        this.setState(prevState => {
            return {iconIsActive: !prevState.iconIsActive}
        });
    };
    render() {
        const { iconIsActive } = this.state;
        return (
            <TouchableOpacity onPress={() => this.toggleIcon()} style={this.props.style}>
                <Icon
                    size={this.props.size || 30}
                    name={iconIsActive ? 'bookmark' : 'bookmark-o'}
                    color={iconIsActive ? colors.red : colors.white}/>
                <Icon
                    size={this.props.size || 30}
                    name={'bookmark-o'}
                    color={colors.white}
                    style={[{
                        display: iconIsActive ? 'flex' : 'none' },
                        styles.outline
                    ]}/>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    outline: {
        position: 'absolute',
        top: 0,
        left: 0
    }
});

BookmarkButton.propTypes = {
    iconIsActive: PropTypes.bool.isRequired,
    target: PropTypes.object.isRequired,
};
