import {StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../styles/colors';
import React from 'react';
export default props => (
    <View>
        {!props.isReaded && (
            <View>
                <Icon style={{position: 'absolute', top: 5, right: 5}} color={'red'} name={'circle'}/>
                <Icon
                    name={'circle-o'}
                    color={colors.white}
                    style={styles.outline}/>
            </View>
        )}
    </View>
)
const styles = StyleSheet.create({
    outline: {
        display: 'flex',
        top: 5,
        right: 5
    }
});
