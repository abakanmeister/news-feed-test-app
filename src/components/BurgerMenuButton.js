import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons';

export default class BurgerMenuButton extends React.Component {
    state = {
        isOpen: false
    };

    _handleDrawer = () => {
        this.state.isOpen ? this.props.nav.navigate('DrawerClose') : this.props.nav.navigate('DrawerOpen');
        this.setState({isOpen: !this.state.isOpen});
    };

    render() {
        return (
            <TouchableOpacity
                onPress = {this._handleDrawer} >
                <Icon name={'bars'} size={20} />
            </TouchableOpacity>
        );
    }
}
