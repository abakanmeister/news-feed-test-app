import moment from 'moment';
import { Platform } from 'react-native';
import NetInfo from "@react-native-community/netinfo";

export const getFormattedDateString = dateString => moment(dateString).format('h:mm a, MMM DD YYYY');

export const removeHtmlTagsFromString = string => string.replace(/(<([^>]+)>)/ig, '');

export const checkConnection = () => {
    function isNetworkConnected() {
        if (Platform.OS === 'ios') {
            return new Promise(resolve => {
                const handleFirstConnectivityChangeIOS = isConnected => {
                    NetInfo.removeEventListener('change', handleFirstConnectivityChangeIOS);
                    resolve(isConnected);
                };
                NetInfo.addEventListener('change', handleFirstConnectivityChangeIOS);
            });
        }
        return NetInfo.fetch();
    }
    return isNetworkConnected().done(isConnected => isConnected)
};
