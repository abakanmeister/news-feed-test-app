import React from 'react';
import {
    SafeAreaView,
} from 'react-native';
import AppNavigator from './navigation';
import OfflineNotice from './components/OfflineNotice';

console.disableYellowBox = true;
export default class App extends React.Component {
    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <AppNavigator />
                <OfflineNotice />
            </SafeAreaView>
        );
    }
}

