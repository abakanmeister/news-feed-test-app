import React from 'react';
import {View} from 'react-native';
import { connect } from 'react-redux';
import newsStyles from '../../news/styles';
import BookmarksList from '../components/BookmarksList';
import { selectNews, readNews } from '../../news/actions';

class BookmarksListScreen extends React.Component {
    showNewsDetails = item => {
        const { selectNews, navigation, readNews } = this.props;
        selectNews(item);
        readNews(item.id);
        navigation.navigate('BookmarksDetails');
    };
    render() {
        const { bookmarks, navigation } = this.props;
        return (
            <View style={newsStyles.screenView}>
                <BookmarksList navigation={navigation}
                               showNewsDetails={this.showNewsDetails}
                               bookmarks={bookmarks}/>
            </View>
        );
    }
}
const mapStateToProps = state => ({
    bookmarks: state.bookmarks.items
});
export default connect(mapStateToProps, {selectNews, readNews})(BookmarksListScreen);
