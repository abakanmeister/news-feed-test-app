import React from 'react';
import {getFormattedDateString, removeHtmlTagsFromString} from '../../../helpers';
import {FlatList, Image, TouchableOpacity, View} from 'react-native';
import MyButton from '../../../components/MyButton';
import newsStyles from '../../news/styles';
import MyText from '../../../components/MyText';
import {addBookmark, removeBookmark} from '../actions';
import BookmarkButton from '../../../components/BookmarkButton';

export default class BookmarksList extends React.Component {
    renderItem = ({item, index}) => {
        const dateToShow = getFormattedDateString(item.date),
            image = {uri: item.imageUrl},
            { showNewsDetails, bookmarks } = this.props,
            isInBookmark = bookmarks.find(bookmark => bookmark.id === item.id);
        return (
            <View>
                <TouchableOpacity style={[newsStyles.card, index === 0 && {marginTop: 10}]}
                                  onPress={() => showNewsDetails(item)}>
                    <View style={newsStyles.cardContentWrapper}>
                        <View style={{width: '30%', height: '100%'}}>
                            <Image style={newsStyles.image} source={image}/>
                            <BookmarkButton style={{position: 'absolute', top: 5, right: 7}}
                                            removeBookmark={removeBookmark(item.id)}
                                            addBookmark={addBookmark}
                                            iconIsActive={!!isInBookmark}
                                            target={item} />
                        </View>
                        <View style={[{flex: 2},newsStyles.cardContentPadding]}>
                            <View style={{marginBottom: 5}}>
                                <MyText style={newsStyles.title}>{item.title}</MyText>
                                <MyText style={newsStyles.shortDescription}>{removeHtmlTagsFromString(item.shortDescription)}</MyText>
                                <MyText style={newsStyles.date}>{dateToShow}</MyText>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    };
    render() {
        const { bookmarks } = this.props;
        return (
            <View>
                <FlatList data={bookmarks}
                          renderItem={this.renderItem}
                          refreshing={false}
                          onRefresh={() => this.onRefresh()}
                          keyExtractor={(item, index) => `list-item-${index}`} />
            </View>
        )
    }
}
