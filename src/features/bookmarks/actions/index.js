import * as types from './types';

export const addBookmark = news => dispatch => {
    console.log('action addBookmark', news);
    dispatch({
        type: types.ADD_BOOKMARK,
        payload: news
    })
};

export const removeBookmark = id => dispatch => {
    console.log('action removeBookmark', id);
    dispatch({
        type: types.REMOVE_BOOKMARK,
        payload: id
    })
};
