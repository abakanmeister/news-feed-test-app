import * as types from './types';

export const fetchNews = sources => dispatch => {
    dispatch({
        type: types.NEWS_FETCHING,
        payload: {}
    });
    console.log('action - fetchNews', 'sources -> ', sources);
    Promise.all(sources.map(source => fetch(source.value)))
    .then(resp => Promise.all(resp.map(r => r.text())))
    .then(texts => {
        let output = [];
        texts.forEach(text => {
            text = JSON.parse(text);
            output = [...output, ...text.feed.article];
        });
        dispatch({
            type: types.ADD_NEWS,
            payload: output
        });
    })
    .catch(e => {
        console.log('CATCH!!!!!!!!!!!');
        dispatch({
            type: types.NEWS_FETCHING_FAILED,
            payload: {}
        })
    });
};

export const selectNews = id => dispatch => {
    dispatch({
        type: types.SELECT_NEWS,
        payload: id
    });
};

export const addSource = url => dispatch => {
    dispatch({
        type: types.ADD_SOURCE,
        payload: url
    });
};

export const deleteSource = url => dispatch => {
    dispatch({
        type: types.DELETE_SOURCE,
        payload: url
    });
};

export const readNews = id => dispatch => {
    console.log("read news" , id);
    dispatch({
        type: types.READ_NEWS,
        payload: id
    });
};

