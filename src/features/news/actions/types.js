export const ADD_NEWS = 'ADD_NEWS';
export const SELECT_NEWS = 'SELECT_NEWS';
export const NEWS_FETCHING = 'NEWS_FETCHING';
export const NEWS_FETCHING_FAILED = 'NEWS_FETCHING_FAILED';
export const ADD_SOURCE = 'ADD_SOURCE';
export const DELETE_SOURCE = 'DELETE_SOURCE';
export const READ_NEWS = 'READ_NEWS';
