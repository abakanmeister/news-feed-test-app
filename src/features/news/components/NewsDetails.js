import React from 'react';
import { View, StyleSheet, Image, Linking } from 'react-native';
import MyText from '../../../components/MyText'
import MyButton from '../../../components/MyButton'
import colors from '../../../styles/colors';
import { SSP_bold } from '../../../styles/fonts';
import newsStyles from '../styles';
import {getFormattedDateString, removeHtmlTagsFromString} from '../../../helpers';
import {removeBookmark, addBookmark} from '../../bookmarks/actions';
import BookmarkButton from '../../../components/BookmarkButton';

export default props => {
    const { item, bookmarks } = props,
        image = {uri: item.imageUrl},
        dateToShow = getFormattedDateString(item.date),
        isInBookmark = props.bookmarks.find(bookmark => bookmark.id === item.id);
    return (
        <View style={styles.container}>
            <MyText style={styles.title}>{removeHtmlTagsFromString(item.title)}</MyText>
            <Image source={image} resizeMode={'contain'} style={{height: 200, width: '100%'}} />
            <BookmarkButton style={{position: 'absolute', top: 5, right: 7}}
                            removeBookmark={removeBookmark}
                            addBookmark={addBookmark}
                            iconIsActive={!!isInBookmark}
                            target={item} />
            <View style={styles.contentMargin}>
                <MyText style={newsStyles.shortDescription}>{removeHtmlTagsFromString(item.description)}</MyText>
                <View style={{borderBottomWidth: 1, borderColor: colors.black, width: '45%', alignSelf: 'flex-end', marginTop: 10, borderRadius: 5, borderStyle: 'dashed'}}></View>
                <MyText style={[newsStyles.date, {color: colors.black}]}>{dateToShow}</MyText>
                <MyButton title={item.link ? 'Open link' : 'No link'} style={!item.link && {backgroundColor: 'lightgray'}} disabled={!!!item.link} onPress={() => item.link && Linking.openURL(item.link)} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    title: {
        textAlign: 'center',
        fontSize: 18,
        fontFamily: SSP_bold,
        marginBottom: 10
    },
    contentMargin: {
        margin: 10
    }
});
