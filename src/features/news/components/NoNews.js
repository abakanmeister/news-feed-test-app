import React from 'react';
import {View, StyleSheet} from 'react-native';

import MyButton from '../../../components/MyButton';
import MyText from '../../../components/MyText';
import {SSP_Regular} from '../../../styles/fonts';

export default class NoNews extends React.Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <MyText style={{fontFamily: SSP_Regular}}>The source is not defined</MyText>
                <View style={styles.buttonWrapper}>
                    <MyButton onPress={() => navigate('SetupSource')}
                            title={'Setup the source'} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonWrapper: {
        alignItems: 'center',
        marginTop: 10
    },
    container: {
        flex: 1,
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
