import React from 'react';
import { View, TouchableOpacity, FlatList, Image, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';

import { getFormattedDateString, removeHtmlTagsFromString, checkConnection } from '../../../helpers';
import MyButton from '../../../components/MyButton';
import MyText from '../../../components/MyText';
import newsStyles from '../styles';
import BookmarkButton from '../../../components/BookmarkButton';
import colors from '../../../styles/colors';
import NoReadIndicator from '../../../components/NoReadIndicator';
import { addBookmark, removeBookmark } from '../../bookmarks/actions';

class NewsList extends React.Component {
    constructor(props) {
        super(props);
        this.page = 1;
        this.state = {
            isRefreshing: false, // for pull to refresh
            error: '',
            kek: false
        }
    }
    handleRefresh = () => {
        const { fetchNews, sources } = this.props;
        fetchNews(sources);
    };
    renderItem = ({item, index}) => {
        const dateToShow = getFormattedDateString(item.date),
            image = {uri: item.imageUrl},
            { navigation, showNewsDetails, readed, bookmarks, addBookmark, removeBookmark } = this.props,
            isReaded = readed.has(item.id),
            { kek } = this.state,
            isInBookmark = !!bookmarks.find(bookmark => bookmark.id === item.id);
        return (
            <View>
                {index === 0 && (
                    <View style={styles.buttonWrapper}>
                        <MyButton onPress={() => navigation.navigate('SetupSource')}
                                  title={'Add source'} />
                    </View>
                )}
                <TouchableOpacity style={[newsStyles.card, index === 0 && {marginTop: 10}]}
                                  onPress={() => showNewsDetails(item)}>
                    <View style={newsStyles.cardContentWrapper}>
                        <View style={{width: '30%', height: '100%'}}>
                            <Image style={newsStyles.image} source={image}/>
                            <BookmarkButton style={{position: 'absolute', top: 5, right: 7}}
                                            removeBookmark={removeBookmark}
                                            addBookmark={addBookmark}
                                            iconIsActive={!!isInBookmark}
                                            target={item} />
                        </View>
                        <View style={[{flex: 2},newsStyles.cardContentPadding]}>
                            <View style={{marginBottom: 5}}>
                                <MyText style={newsStyles.title}>{removeHtmlTagsFromString(item.title)}</MyText>
                                <MyText style={newsStyles.shortDescription}>{removeHtmlTagsFromString(item.shortDescription)}</MyText>
                                <MyText style={newsStyles.date}>{dateToShow}</MyText>
                            </View>
                        </View>
                        {/*<NoReadIndicator isReaded={isReaded} />*/}
                        <NoReadIndicator isReaded={kek} />
                    </View>
                </TouchableOpacity>
            </View>
        )
    };
    onRefresh = () => console.log('refreshing!');
    render() {
        const { news, navigation } = this.props,
            newsDefined = !!news.length,
            { isRefreshing } = this.state;
        console.log('render NewsList', this.props.readed);
        return (
            <View>
                {newsDefined &&
                    <FlatList data={news}
                              extraData={this.props}
                              renderItem={this.renderItem}
                              refreshing={isRefreshing}
                              onRefresh={() => this.handleRefresh()}
                              keyExtractor={(item, index) => `list-item-${index}`} />
                }
                {!newsDefined &&
                    <View style={styles.buttonWrapper}>
                        <MyButton onPress={() => navigation.navigate('SetupSource')}
                                  title={'Setup the source'} />
                    </View>
                }
            </View>
        );
    }
}
const mapStateToProps = state => ({
    news: state.news.items,
    bookmarks: state.bookmarks.items,
    source: state.news.sources,
    readed: state.news.readed
});
export default connect(mapStateToProps, {addBookmark, removeBookmark})(NewsList);
const styles = StyleSheet.create({
    buttonWrapper: {
        alignItems: 'center',
        marginTop: 10
    }
});
