import * as types from '../actions/types';
import moment from 'moment';

const initialState = {
    items: [],
    selected: null,
    isFetching: false,
    fetchingFailed: false,
    // sources: [{id: 1, value: ''}],
    sources: [{id: 1, value: 'https://gist.githubusercontent.com/happy-thorny/bd038afd981be300ac2ed6e5a8ad9f3c/raw/dd90f04475a2a7c1110151aacc498eabe683dfe4/memes.json'},
        {id: 2, value: 'https://gist.githubusercontent.com/happy-thorny/bd038afd981be300ac2ed6e5a8ad9f3c/raw/dd90f04475a2a7c1110151aacc498eabe683dfe4/memes.json'}],
    readed: new Set()
};

export default (state = initialState, action) => {
    const { payload:income } = action;
    switch (action.type) {
        case types.ADD_NEWS: {
            const { items } = state,
                lastId = items.length ? items.reverse()[0].id +1 : 1,
                sortedIncomeValuesWithIds = income
                    .sort((a, b) => moment(a.date) - moment(b.date) > 0 ? -1 : 1)
                    .map((val, index) => {return {id: lastId +index, ...val}});
            const newValues = [...items, ...sortedIncomeValuesWithIds];
            return {
                ...state,
                items: newValues,
                isFetching: false,
                fetchingFailed: false
            };
        }
        case types.SELECT_NEWS: {
            return {
                ...state,
                selected: income
            };
        }
        case types.NEWS_FETCHING: {
            return {
                ...state,
                isFetching: true,
                fetchingFailed: false
            };
        }
        case types.NEWS_FETCHING_FAILED: {
            return {
                ...state,
                isFetching: false,
                fetchingFailed: true
            };
        }
        case types.ADD_SOURCE: {
            const { sources } = state;
            sources.add(income);
            return {
                ...state,
                sources
            };
        }
        case types.DELETE_SOURCE: {
            const { sources } = state;
            return {
                ...state,
                sources: sources.filter(source => source !== income)
            };
        }
        case types.READ_NEWS: {
            const { readed } = state;
            readed.add(income);
            return {
                ...state,
                readed
            };
        }
        default:
            return state;
    }
};
