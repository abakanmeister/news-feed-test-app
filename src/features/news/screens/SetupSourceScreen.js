import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

import newsStyles from '../styles';
import MyButton from '../../../components/MyButton';
import MyText from '../../../components/MyText';
import MyTextInput from '../../../components/MyTextInput';
import { fetchNews, addSource, deleteSource } from '../actions';
import {SSP_bold, SSP_Regular} from '../../../styles/fonts';
import Icon from 'react-native-vector-icons/FontAwesome';

class SetupSourceScreen extends React.Component {
    constructor(props) {
        super(props);
        const addMode = 'addMode' in props.navigation.state;
        this.state = {
            sources: props.sources,
            addMode
        }
    }
    addNewSourceInput = () => {
        this.setState(prevState => {
            const { sources } = prevState,
                nextId = sources.reverse()[0].id + 1;
            sources.push({id: nextId, value: ''});
            console.log('new added', sources);
            return {sources: sources};
        })
    };
    deleteSource = needleId => {
        console.log('delete source id: ', needleId, this.state.sources);
        this.setState(prevState => {
            let { sources } = prevState;
            sources = sources.filter(source => source.id !== needleId);
            console.log('after delete', sources);
            return {sources: sources};
        })
    };
    setupSourceButton = url => {
        const { addMode, sources } = this.state,
            { addSource, fetchNews } = this.props;
        addMode && addSource(url);
        fetchNews(sources);
    };
    updateItemInStateArr = (value, needleId) => {
        this.setState(prevState => {
            const { sources } = prevState;
            sources.map(source => {
                if (source.id === needleId) return {...source, value};
                return source;
            });
            return sources;
        });
    };
    render() {
        const { sources } = this.state;
        return (
            <View style={newsStyles.screenView}>
                <View style={styles.container}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <MyText style={styles.title}>Enter source URL</MyText>
                        <TouchableOpacity style={{alignSelf: 'center'}}
                                          onPress={() => this.addNewSourceInput()}>
                            <Icon name={'plus'} size={20}/>
                        </TouchableOpacity>
                    </View>
                    {sources.map(source => {
                            const delAvailable = sources.length > 1;
                            return <View style={{flexDirection: 'row', justifyContent: 'space-between'}}
                                         key={source.id}>
                                <MyTextInput onChangeText={value => this.updateItemInStateArr(value, source.id)}
                                             value={source.value}
                                             placeholder={delAvailable.toString()}
                                             style={styles.textInput}/>

                                <TouchableOpacity style={{alignSelf: 'center'}}
                                                  onPress={() => delAvailable ? this.deleteSource(source.id) : this.addNewSourceInput()}>
                                    <Icon name={delAvailable ? 'times' : ''} size={20}/>
                                </TouchableOpacity>
                            </View>
                    })}
                    <View style={styles.buttonWrapper}>
                        <MyButton onPress={() => {this.setupSourceButton(sources)}}
                                  title={'Setup the source'} />
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    isFetching: state.news.isFetching,
    fetchingFailed: state.news.fetchingFailed,
    sources: [...state.news.sources.values()]
});

export default connect (mapStateToProps, {fetchNews, addSource, deleteSource})(SetupSourceScreen);

const styles = StyleSheet.create({
    textInput: {
        width: '90%',
        fontFamily: SSP_Regular
    },
    buttonWrapper: {
        alignItems: 'center',
        marginTop: 10
    },
    container: {
        margin: 10
    },
    title: {
        textAlign: 'center',
        fontSize: 18,
        fontFamily: SSP_bold
    }
});
