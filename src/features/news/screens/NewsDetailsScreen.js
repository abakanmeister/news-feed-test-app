import React from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

import colors from '../../../styles/colors';
import NewsDetails from '../components/NewsDetails';

class NewsDetailsScreen extends React.Component {
    render() {
        const { selected, bookmarks } = this.props;
        return (
            <View style={{flex:1, backgroundColor: colors.contentWrapper}}>
                <View style={{marginTop: 10}}>
                    <NewsDetails item={selected} bookmarks={bookmarks}/>
                </View>
            </View>
        );
    }
}
const mapStateToProps = state => ({
    selected: state.news.selected,
    bookmarks: state.bookmarks.items
});
export default connect(mapStateToProps)(NewsDetailsScreen);
