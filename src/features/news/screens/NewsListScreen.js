import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

import { fetchNews, selectNews, readNews } from '../actions';
import { addBookmark, removeBookmark } from '../../bookmarks/actions';
import NewsList from '../components/NewsList';
import NoNews from '../components/NoNews';
import Spinner from 'react-native-loading-spinner-overlay';
import newsStyles from '../styles';
import store from '../../../redux/configureStore';

class NewsListScreen extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        console.log(store.getState());
    }
    showNewsDetails = item => {
        const { selectNews, navigation, readNews } = this.props;
        selectNews(item);
        readNews(item.id);
        navigation.navigate('NewsDetails');
    };
    render() {
        const { news, isFetching, navigation, fetchNews, sources, bookmarks, readed } = this.props,
            newsDefined = !!news.length;
        return (
            <View style={newsStyles.screenView}>
                <Spinner visible={isFetching} textContent={'loading...'}/>
                {!isFetching && newsDefined
                    ? <NewsList showNewsDetails={this.showNewsDetails}
                                navigation={navigation}
                                sources={sources}
                                news={news}
                                bookmarks={bookmarks}
                                readed={readed}
                                fetchNews={fetchNews} />
                    : <NoNews navigation={navigation} />
                }
            </View>
        );
    }
}
const mapStateToProps = state => ({
    news: state.news.items,
    isFetching: state.news.isFetching,
    readed: state.news.readed,
    bookmarks: state.bookmarks.items,
    sources: state.news.sources
});
export default connect(mapStateToProps, {fetchNews, selectNews, readNews, addBookmark, removeBookmark})(NewsListScreen);
