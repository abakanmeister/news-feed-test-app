import { SSP_bold, SSP_Regular, SSP_SemiBold } from '../../../styles/fonts';
import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';

export default StyleSheet.create({
    title: {
        fontSize: 16,
        textTransform: 'uppercase',
        fontFamily: SSP_bold
    },
    shortDescription: {
        fontFamily: SSP_Regular,
        fontSize: 16
    },
    image: {
        // height: 100,
        width: '100%', height: '100%',
        // minHeight: 100,
        alignSelf: 'center'
    },
    card: {
        flex: 1,
        backgroundColor: 'lightgray',
        borderRadius: 5,
        padding: 5,
        marginBottom: 15,
        elevation: 5,
        marginHorizontal: 10,
    },
    cardContentWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardContentPadding: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    date: {
        fontSize: 12,
        fontStyle: 'italic',
        textAlign: 'right',
        color: colors.white,
        fontFamily: SSP_SemiBold
    },
    screenView: {
        flex:1,
        backgroundColor: colors.contentWrapper
    }
});
