import {applyMiddleware, compose, createStore} from 'redux';
import combinedReducers from '../redux/combinedReducers';
import thunk from 'redux-thunk';
const middleware = [thunk],
    composeEnhancers = compose;
const configureStore = createStore(
    combinedReducers,
    {},
    composeEnhancers (
        applyMiddleware(...middleware)
    )
);

export default configureStore;
