import { combineReducers } from 'redux';
import newsReducer from '../features/news/reducer';
import bookmarksReducer from '../features/bookmarks/reducer';

const appReducer = combineReducers({
    news: newsReducer,
    bookmarks: bookmarksReducer
});

const rootReducer = (state, action) => {
    return appReducer(state, action);
};

export default rootReducer;
