import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import App from './src/App';
import { name as appName } from './app.json';
import store, {persistor} from './src/redux/configureStore';
import {PersistGate} from "redux-persist/integration/react";


const redApp = () => <Provider store={store}><App/></Provider>;
AppRegistry.registerComponent(appName, () => redApp);
